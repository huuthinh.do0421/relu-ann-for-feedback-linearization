function y=Ellipsoid_patch2D(P,e,c,varargin)
%
%  Draw a patch region inside the following ellipsoid in 2D:
%  E =  {x : (x-c)'P(x-c)<=e  }
%  DO Huu-Thinh
%
th = linspace(0,2*pi,100);
xunit = cos(th);
yunit = sin(th);
a=sqrt(e);
X_unit=[xunit;yunit];
BB=find_transform(P);
X=a*BB*X_unit+c;
if nargin ==3
    y=patch(X(1,:), X(2,:),[0 0 1],'facealpha',0.05);
else
    if nargin >3
        y=patch('XData',X(1,:),'YData', X(2,:),varargin{:});
    end
end

end


function B = find_transform(Q)
[U,S,V] = svd(Q);
B = ((U*sqrt(S)*V')')^-1;
end