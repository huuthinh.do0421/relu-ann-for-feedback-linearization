% Dev: Huu-Thinh DO
% This class transforms a ReLU-ANN to its equivalent MIP form.
% Details can be found in: 
% ReLU Networks as Surrogate Models in Mixed-Integer Linear Programs
% Bjarne Grimstad, Henrik Andersson (2019)
classdef MIP_ReLu< handle
    properties
        UpBound; LowBound
        LayerSizes; LayerWeights; LayerBiases;
        BoundIn; BoundOut;
        nIn; nOut; %Dimensions of the input and output
        K; %The number of hidden layers +1
        %Notation can be found here
        %ReLU networks as surrogate models in mixed-integer linear programs
    end
    methods (Access = public)
        function obj = MIP_ReLu(RNN,BoundIn,BoundOut)
            obj.LayerWeights =  RNN.LayerWeights;
            obj.LayerBiases = RNN.LayerBiases;
            obj.LayerSizes = RNN.LayerSizes;
            obj.BoundIn=BoundIn;
            obj.BoundOut=BoundOut;
            obj.K = size(obj.LayerSizes,2)+1;
            obj.nIn = size(RNN.X,2);
            obj.nOut = size(RNN.Y,2);
            obj.CheckInit();
            obj.UpBound = obj.boundInit();
            obj.LowBound = obj.boundInit();
        end
        function out = boundInit(obj)
            out = cell(1,size(obj.LayerSizes,2));
            for i=1:numel(out)
                out{1,i} = zeros(obj.LayerSizes(i),1);
            end
        end
        function CheckInit(obj)
            assert(isequal(size(obj.BoundIn), [obj.nIn, 2]),...
                'Input bounds in wrong dimension') % upper and lower bound for input
            assert(isequal(size(obj.BoundOut), [obj.nOut, 2]),...
                'Output bounds in wrong dimension') % upper and lower bound for output
        end
        function IABoundTighten(obj)
            err = 0;
            W = obj.LayerWeights;
            b = obj.LayerWeights;
            %% ========== Special case for k=1 =================
            k =1; % k = 1
            nkm1 = obj.nIn;
            U_km1 = obj.BoundIn(:,2);
            L_km1 = obj.BoundIn(:,1);
            for j=1:obj.LayerSizes(k)
                ujk=0;
                ljk=0;
                for i=1:nkm1 %(n_{k-1} = n_0 = nIn)
                    %%%%%%%%%%%% Upper bound %%%%%%%%%%%%
                    utmp = max(...
                        W{k}(j,i)*U_km1(i),...
                        W{k}(j,i)*L_km1(i)...
                        );
                    ujk = ujk + utmp;
                    %%%%%%%%%%%% Lower bound %%%%%%%%%%%%
                    ltmp = min(...
                        W{k}(j,i)*U_km1(i),...
                        W{k}(j,i)*L_km1(i)...
                        );
                    ljk = ljk + ltmp;
                end
                obj.UpBound{k}(j) = ujk + b{k}(j) + err;
                obj.LowBound{k}(j) = ljk + b{k}(j) - err;
            end
            %% ========== k>=2 ==================================
            ujk=0;
            ljk=0;
            for k=2:obj.K-1
                nkm1 = obj.LayerSizes(k-1);
                U_km1 = obj.UpBound{k-1};
                L_km1 = obj.LowBound{k-1};
                for j=1:obj.LayerSizes(k)
                    ujk=0;
                    ljk=0;
                    for i=1:nkm1 %(n_{k-1})
                        %%%%%%%%%%%% Upper bound %%%%%%%%%%%%
                        utmp = max(...
                            W{k}(j,i)*max(0,U_km1(i)),...
                            W{k}(j,i)*max(0,L_km1(i))...
                            );
                        ujk = ujk + utmp;
                        %%%%%%%%%%%% Lower bound %%%%%%%%%%%%
                        ltmp = min(...
                            W{k}(j,i)*max(0,U_km1(i)),...
                            W{k}(j,i)*max(0,L_km1(i))...
                            );
                        ljk = ljk + ltmp;
                    end
                    obj.UpBound{k}(j) = ujk + b{k}(j) +err;
                    obj.LowBound{k}(j) = ljk + b{k}(j) -err;
                end
            end
        end
    end
end