function f = ackley(x)
% Dimension
n = numel(x);
x = [3*x(1) x(2)];
% Ackley function
f = 20 + exp(1) ...
-20*exp(-0.2*sqrt((1/n)*(sum(x.^2)) )) ...
-exp((1/n)*sum(cos(2*pi*x)));

