function dB = gradientCBF(X,Xobs)
x1 = X(1,1); x2 = X(2,1);
x1obs=Xobs(1);x2obs=Xobs(2);
dB = [(2*x1 - 2*x1obs)/(2*((x1 - x1obs)^2 + (x2 - x2obs)^2)^(1/2));
(2*x2 - 2*x2obs)/(2*((x1 - x1obs)^2 + (x2 - x2obs)^2)^(1/2))];
end
