function tau = FL_twolink(theta)
% Introduction to Robotics: Mechanics and Control Third Edition
% Craig John J
% theta = [theta1 dot_theta1 v1 theta2 dot_theta2 v2]^T;
theta1 = theta(1);
dot_theta1 = theta(2);
v1 = theta(3);

theta2 = theta(4);
dot_theta2 = theta(5);
v2 = theta(6);


%% constants
g=9.81;


m1 = 0.25;
l1 = 3; 
% lc1=l1/2;
% I1 = (1/12)*m1*l1^2;

m2 = m1;
l2 = 2; 
% lc2=l2/2;
% I2 = (1/12)*m2*l2^2;


%% Matrix component
M11 = l2^2*m2 +2*l1*l2*m2*cos(theta2) + l1^2*(m1+m2);
M12 = l2^2*m2 + l1*l2*m2*cos(theta2) ;
M22 = l2^2*m2;
M=[M11, M12; M12, M22];
V = [-m2*l1*l2*sin(theta2)*dot_theta2^2 - 2*m2*l1*l2*sin(theta2)*dot_theta1*dot_theta2;
    m2*l1*l2*sin(theta2)*dot_theta1^2];
G = [m2*l2*g*cos(theta1+theta2) + (m1+m2)*l1*g*cos(theta1);
    m2*l2*g*cos(theta1+theta2)];


tau = M * [v1;v2] +V +G;
end