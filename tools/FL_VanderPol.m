function y=FL_VanderPol(Z)
x1=Z(1); x2 = Z(2); v=Z(3);
w=2; eta=0.7;

y = v - 2*w*eta*(1-x1^2)*x2 + w^2*x1;
end