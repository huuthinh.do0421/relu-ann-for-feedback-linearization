% Dev: Huu-Thinh DO
% This class creates a ReLU-ANN object with its conventional feedforward
% representation. This should be used when the ANN toolbox from MATLAB is
% not available. 
classdef MyRNN < handle
    properties
        LayerSizes; LayerWeights; LayerBiases;
        nIn; nOut; %Dimensions of the input and output
        Xhidden;Xin;Xout;
        K; %The number of hidden layers +1
        %Notation can be found here
        %ReLU networks as surrogate models in mixed-integer linear programs
    end
    methods (Access = public)
        function obj = MyRNN(RNN)
            obj.LayerWeights =  RNN.LayerWeights;
            obj.LayerBiases = RNN.LayerBiases;
            obj.LayerSizes = RNN.LayerSizes;
            obj.K = size(obj.LayerSizes,2)+1;
            obj.nIn = size(RNN.X,2);
            obj.nOut = size(RNN.Y,2);
            obj.Xhidden = obj.HiddenLayerInit();
            obj.Xin = nan;
            obj.Xout = nan;
        end
        function outX = eval(obj,inX)
            for k=1:obj.K-1
                if k==1
                    obj.Xhidden{k}=vecRelu(obj.LayerWeights{k}*inX+...
                        obj.LayerBiases{k});
                else
                    obj.Xhidden{k}=vecRelu(obj.LayerWeights{k}*obj.Xhidden{k-1}+...
                        obj.LayerBiases{k});
                end
            end
            outX = obj.LayerWeights{obj.K}*obj.Xhidden{obj.K-1}+...
                obj.LayerBiases{obj.K};
            obj.Xin = inX;
            obj.Xout = outX;
        end
        function out = HiddenLayerInit(obj)
            out = cell(1,obj.K-1);
            for i=1:numel(out)
                out{1,i} = zeros(obj.LayerSizes(i),1);
            end
        end
        
    end
    
    
end

function y=vecRelu(x)
if isequal(size(x),[1 1])
   y=max(0,x);
else
   y=x;
    for i=1:numel(x)
        y(i) = max(0,x(i));
    end
end
end