% https://arxiv.org/pdf/2010.09819.pdf
%the CBF
function h = CBF(X,Xobs,Robs)
x1 = X(1,1); x2 = X(2,1);
x1obs=Xobs(1); x2obs=Xobs(2);

h = sqrt( (x1-x1obs)^2 + (x2-x2obs)^2 ) - Robs;

end
