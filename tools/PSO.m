%Class of particles
classdef PSO < handle
    properties (SetAccess = private)
        N,np,bounds,g_best_position, g_best_objective,...
            acc_constant,particle_list,terminate_check,iter,iter_max ;
    end
    methods (Access = public)
        function obj = PSO(init)
            obj.iter = 0;
            obj.iter_max = init.K_max;
            obj.N = init.N; % number of particle
            obj.np = size(init.bounds.A,2); % dimension of the particle (size)
            obj.acc_constant = init.acc_constant;
            obj.particle_list = cell(1,obj.N);
            obj.bounds = init.bounds;

            rand_init_pts = cprnd(obj.N, obj.bounds.A,obj.bounds.b)';
            
            obj.g_best_position = [];
            obj.g_best_objective = [];
            
            for i = 1:obj.N
                obj.particle_list{i} = particle(init.bounds, obj.np,...
                    rand_init_pts(:,i),obj.acc_constant,init.K_max,...
                    init.RNN,init.TrueFunc,init.epsilon,init);
                if i ==1
                    obj.g_best_position = obj.particle_list{i}.p_best_position;
                    obj.g_best_objective = obj.particle_list{i}.p_best_objective;
                else
                    if  obj.g_best_objective <= obj.particle_list{i}.p_best_objective
                        obj.g_best_position = obj.particle_list{i}.p_best_position;
                        obj.g_best_objective = obj.particle_list{i}.p_best_objective;
                    end
                    
                end
                
            end
        end
        
        function runPSO(obj)
            obj.iter = obj.iter+1;
            for i = 1:obj.N
                obj.particle_list{i}.update_velocity(obj.g_best_position);
                obj.particle_list{i}.update_position();
                obj.particle_list{i}.eval();
                
                if  obj.g_best_objective <= obj.particle_list{i}.p_best_objective
                    obj.g_best_position = obj.particle_list{i}.p_best_position;
                    obj.g_best_objective = obj.particle_list{i}.p_best_objective;
                end
            end
            
        end
    end
end

