function u = FL_quad1D(Z)
yd=Z(1);y2d=Z(2);y3d=Z(3);
tau = 0.2; Gam = 10; gam = 0.3;
u = ((tau/Gam)*(y3d+gam*y2d)/sqrt(1-Gam^(-2)*(y2d+gam*yd)^2))...
    + asin(Gam^(-1)*(y2d+gam*yd));
end