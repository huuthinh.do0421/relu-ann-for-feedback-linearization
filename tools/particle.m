% Class of particles
% Objective: maximize the fitness/objective
% A Generic Particle Swarm Optimization Matlab Function

classdef particle< handle
    properties (SetAccess = private)
        bounds, np, position, velocity, p_best_position, p_best_objective, ...
        objective, k, acc_constant, phi_a, phi_b , K_max,...
        RNN,TrueFunction; 
        distance_to_global_best, reInitFlag, reInitRadius, arrive2Global;
    end   
    methods (Access = public)
        function obj = particle(bounds, np, position_init,...
                acc_constant,K_max,RNN, TrueFunction,epsilon,init) % initialization
            obj.np = np; %the vector size
            obj.bounds = bounds; % linear wall for particles
            obj.RNN = RNN;
            % particle's props
            % functions
            obj.TrueFunction = TrueFunction;
            obj.position = position_init;
            obj.velocity = cprnd(1,bounds.A,bounds.b)'*0;
            obj.p_best_position = position_init;
            obj.objective =  obj.fitness(obj.position(:,end));
            obj.p_best_objective = obj.fitness(obj.p_best_position);
            obj.k = 0;
            obj.acc_constant = acc_constant;
            
            % for reinitialization
            obj.arrive2Global=false;
            if isfield(init, 'reInitFlag') 
                if init.reInitFlag
                    obj.reInitFlag=true;
                    obj.distance_to_global_best = 1e6;
                    obj.reInitRadius=init.reInitRadius;
                else
                    obj.reInitFlag=false;
                    obj.distance_to_global_best = nan;
                end
            end
            
            % dynamics
            obj.phi_a = 1-epsilon; 
            obj.phi_b = mean(acc_constant)-1+epsilon;
            obj.K_max = K_max;
            
            
            
          end
        
        function eval(obj)
            obj.objective =  [obj.objective, obj.fitness(obj.position(:,end))];
            if obj.p_best_objective < obj.objective(end)
                obj.p_best_position = obj.position(:,end);
                obj.p_best_objective = obj.objective(end);
            end
        end
        
        function update_velocity(obj, global_best_position)
            obj.k = obj.k+1;            
            gamma1 = rand(1,obj.np);
            gamma2 = rand(1,obj.np);
            % random for each dimenstion >>> diag
            v_cognitive = obj.acc_constant(1)*diag(gamma1)*(obj.p_best_position - obj.position(:,end));
            v_social = obj.acc_constant(2)*diag(gamma2)*(global_best_position-obj.position(:,end));
            phi_k = (obj.phi_b-obj.phi_a)*(obj.k-1)/obj.K_max + obj.phi_a;
            vel = phi_k * obj.velocity(:,end) + v_cognitive + v_social;
            
            % reinitialize the position and velocity
            if obj.reInitFlag
                obj.distance_to_global_best = norm(...
                    obj.position(:, end)-global_best_position);
                if obj.distance_to_global_best<=obj.reInitRadius
                    obj.arrive2Global = true;
                end
            end
             
            % update the velocity
            if obj.arrive2Global
                obj.velocity = [obj.velocity, vel*0];
            else
                obj.velocity = [obj.velocity, vel];
            end
        end
        
        function update_position(obj)
            xtmp = obj.position(:,end) + obj.velocity(:,end);
            if ~all(obj.bounds.A*xtmp<=obj.bounds.b) 
                % find closest point in the bounds
                options = optimoptions('quadprog','Display','off');
                xtilde = quadprog(eye(obj.np),zeros(obj.np,1),obj.bounds.A,...
                    obj.bounds.b-obj.bounds.A*xtmp,[],[],[],[],xtmp,options);
                xtmp = xtmp + xtilde;
            end
            if obj.arrive2Global
                obj.position(:, end+1) = cprnd(1,obj.bounds.A,obj.bounds.b)';
%                 disp('reinit');
            else
                obj.position(:, end+1) = xtmp;
            end
        end     
        
        
        
        
        function y=fitness(obj, X)
            y=norm(...
               obj.TrueFunction(X)...
               -obj.RNN.eval(X),2);
        end
    end
end

