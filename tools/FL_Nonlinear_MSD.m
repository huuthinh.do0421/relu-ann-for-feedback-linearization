function u=FL_Nonlinear_MSD(Z)
x1 = Z(1); x2 = Z(2); v = Z(3);
u=v+Stribeck(x2)+x1;
end


function s = Stribeck(x)
s=(0.8+0.2*exp(-100*abs(x)))*tanh(10*x)+x;
end