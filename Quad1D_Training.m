clc
clear
close all
addpath(genpath('tools'))
%% constants
tau = 0.2;
Gam = 10;
gam = 0.3;
%% the feedback linearization map
FL = @(y3d, y2d, yd)((tau/Gam)*(y3d+gam*y2d)/sqrt(1-Gam^(-2)*(y2d+gam*yd)^2))...
    + asin(Gam^(-1)*(y2d+gam*yd));
%% create data
max_box =[5;5;15];
Nsamp = 15;
Box_ws = Polyhedron('ub', max_box,'lb', -max_box);
y3d_train = linspace(-max_box(3),max_box(3),Nsamp);
y2d_train = linspace(-max_box(2),max_box(2),Nsamp);
yd_train = linspace(-max_box(1),max_box(1),Nsamp);
Xtrain = [];
Ytrain = [];
for x1 = yd_train
    for x2 = y2d_train
        for x3 = y3d_train
            ytmp = FL(x3,x2,x1);
            if isreal(ytmp)
                Xtrain = [Xtrain; [x1 x2 x3]];
                Ytrain = [Ytrain;ytmp];
            end
        end
    end
end

%% train the network with fitrnet
HiddenLayerSize=[10 10 10];
Mdl = fitrnet(Xtrain,Ytrain,"Standardize",false, ...
    "LayerSizes",HiddenLayerSize,'verbose',1,'IterationLimit',2e3);
fprintf('training time %.5f (seconds)\n',sum(Mdl.TrainingHistory.Time))
%% sample some random points
clc
XTest =  cprnd(500,Box_ws.A,Box_ws.b);
YTest = [];
YThinh = [];
ThinhRNN = MyRNN(Mdl);
testPredictions=[];
for i=1:size(XTest,1)
    if isreal(FL(XTest(i,3),XTest(i,2),XTest(i,1)))
        YTest = [YTest; FL(XTest(i,3),XTest(i,2),XTest(i,1))];
        YThinh = [YThinh; ThinhRNN.eval(XTest(i,:)')];
        testPredictions=[testPredictions, Mdl.predict(XTest(i,:))];
    end
end

%%
figure
scatter(YTest,testPredictions,15,'filled','markerfacecolor','r')
hold on
plot(YTest,YTest,'b','linewidth',2)
scatter(YTest,YThinh,15,'g')
hold off
xlabel("True value")
ylabel("Approximated value with relu ANN")
%% estimate the error bound with pso
close all
% PSO initialization
init.K_max = 800; % total search steps
init.N = 500; %number of particles
init.bounds = Box_ws;
init.acc_constant = [1 50]*0.001; 
init.RNN  = MyRNN(Mdl);
init.TrueFunc = @FL_quad1D;
init.epsilon = 1e-8;
% reintialization flag
init.reInitFlag=true;
init.reInitRadius=1e-6;

pso_1=PSO(init);
obj_test=[];
figure
hold on
xlim([0 init.K_max])
for i=1:init.K_max
    disp(['loop ' num2str(i)]);
    pso_1.runPSO();
    obj_test = [obj_test, pso_1.g_best_objective];
    scatter(i,obj_test(end),10,'filled','markerfacecolor','b')
    drawnow
    ylabel('Largest error found')
    xlabel('Search step')
end
disp('_________________________________________________')
fprintf('The largest error found %.5f\n',obj_test(end))

