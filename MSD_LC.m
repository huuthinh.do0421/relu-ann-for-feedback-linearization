% Stabilization with guaranteed safety using Control Lyapunov–Barrier Function
% Romdlony et al (2016)
% Example 1
% Linear cost

clc
clear
close all
addpath(genpath('tools'))
% Load the network and compute the bounds
load('data_MSD4_RNN_MIP.mat')
MSD_RNN_MIP.IABoundTighten(); % the bounds


%% systems
A = [0 1;
    0 0];
B = [0;1];
% discretization
Ts=0.02;
nx=size(A,1);
nu = size(B,2);
Ad = (eye(nx) - A*Ts/2)^-1 * (eye(nx) + A*Ts/2);
Bd = (eye(nx) - A*Ts/2)^-1 * B*Ts;
umax=5;
%% Control design
% Some CLF
Q = diag([1 1]);
R = 100;
[~,P,~] = lqr(A,B,Q,R);
beta = 0.001;
[Kopt,~,~] = lqr(A,B,diag([10 1]),0.5);
%% Build the optimizer
X_feedback = sdpvar(nx,1);
K=MSD_RNN_MIP.K;

% CBF parameters
barrier = sdpvar(1,1);
grad_barrier = sdpvar(nx,1);

% input constraints in the new coordinates
vIn = sdpvar(nu,1);
uOut = sdpvar(nu,1);
% delta = sdpvar(1,1);
xHidden = cell(1,numel(MSD_RNN_MIP.LayerSizes));
sHidden = cell(1,numel(MSD_RNN_MIP.LayerSizes));
zHidden = cell(1,numel(MSD_RNN_MIP.LayerSizes)); %binaire
for k =1:K-1
    xHidden{k} = sdpvar(MSD_RNN_MIP.LayerSizes(k),1);
    sHidden{k} = sdpvar(MSD_RNN_MIP.LayerSizes(k),1);
    zHidden{k} = binvar(MSD_RNN_MIP.LayerSizes(k),1);
end
constraints=[];
for k =1:K-1
    if k==1
        constraints = [constraints, ...
            MSD_RNN_MIP.LayerWeights{k}*[X_feedback(1,1);X_feedback(2,1);vIn] + MSD_RNN_MIP.LayerBiases{k} == xHidden{k}-sHidden{k}];
    else
        constraints = [constraints, ...
            MSD_RNN_MIP.LayerWeights{k}*xHidden{k-1} + MSD_RNN_MIP.LayerBiases{k} == xHidden{k}-sHidden{k}];
    end
    constraints = [constraints, xHidden{k}>=0, sHidden{k}>=0];
    for j = 1:numel(xHidden{k}) %n_k
        constraints = [constraints,...
            xHidden{k}(j)<= MSD_RNN_MIP.UpBound{k}(j)*zHidden{k}(j),...
            sHidden{k}(j)<= -MSD_RNN_MIP.LowBound{k}(j)*(1-zHidden{k}(j)),...
            ];
    end
end
constraints = [constraints,...
    MSD_RNN_MIP.LayerWeights{K}*xHidden{K-1} + MSD_RNN_MIP.LayerBiases{K} ==  uOut,...
    uOut<= umax-ep, -(umax-ep)<=uOut];

constraints = [constraints, 2*(P*X_feedback)'*(A*X_feedback + B*vIn)...
    <=-beta*X_feedback'*P*X_feedback ];

gamma = 4;
constraints = [constraints, grad_barrier'*(A*X_feedback + B*vIn)>= -gamma*barrier];

% linear cost
objective = X_feedback'*P*B*vIn  ; %2*(P*X_feedback)'*(A*X_feedback + B*vIn);
options = sdpsettings('solver','cplex','verbose',0);

parameters_opt={X_feedback,barrier,grad_barrier};
output={vIn};
controller=optimizer(constraints,objective,options,parameters_opt,output);

%% simulation
X0 = [0 ;4.];
tsim = 10;
Nsim = round(tsim/Ts);
tt=linspace(0,tsim,Nsim);
Xsim = zeros(nx,Nsim+1);
Xsim(:,1) = X0;
vsim = zeros(nu, Nsim);
usim = zeros(nu, Nsim);
Xo = [1.5; 1];
Ro= 0.8;

for i=1:Nsim
    tic;
    vsim(i)=controller{Xsim(:,i),CBF(Xsim(:,i),Xo,Ro),gradientCBF(Xsim(:,i),Xo)};
    comptime(i)=toc;
    if isnan(vsim(i))
        error('infeasible')
    end
    i
    usim(i) = FL_Nonlinear_MSD([Xsim(1,i),Xsim(2,i),vsim(i)]');
    Xsim(:,i+1) = Ad*Xsim(:,i)+Bd*vsim(i);
end
for i=1:size(Xsim,2)-1
    Vclf(i)=Xsim(:,i)'*P*Xsim(:,i);
end
%%
for i=1:Nsim
    uNN(i) =  MSD_rnn.eval([Xsim(1,i),Xsim(2,i),vsim(i)]');
end
%%
close all
figure
subplot(1,2,1)
scatter(Xsim(1,1:end-1),Xsim(2,1:end-1))
hold on
Ellipsoid_patch2D(eye(2),Ro^2,Xo,'facealpha',0.1);
plot(Xsim(1,1:end-1),Xsim(2,1:end-1))
ylabel('x_2')
xlabel('x_1')


subplot(1,2,2)
plot(tt,Vclf)
ylabel('CLF V(z)')
figure
set(gcf,'color','w');
subplot(3,1,1)
plot(tt, Xsim(1,1:end-1));
hold on
plot(tt, Xsim(2,1:end-1));
% plot(tt, Xsim(3,1:end-1));
legend({'x_1','x_2'})
ylabel('states')
box on

subplot(3,1,2)
stairs(tt,vsim(1,:));
ylabel('virtual input')
box on


subplot(3,1,3)
yline(umax,'color','b','linewidth',1)
hold on
yline(-umax,'color','b','linewidth',1)
plot(tt,usim(1,:),'r','linewidth',2);

stairs(tt,uNN(1,:),'ko-','markersize',3);

ylabel('real input')
xlabel('time (s)')
box on

figure
plot(comptime)
ylabel('computation time (s)')


