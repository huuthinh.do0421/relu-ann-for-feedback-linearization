clc
clear
close all
addpath(genpath('tools'))
%% System
tau = 0.2;
Gam = 10;
gam = 0.3;
umax = 10*pi/180;
% the linearized dynamics
A = [0 1 0;
    0 0 1;
    0 0 0];
B = [0;0;1];

% discretization
Ts=0.1; 
nx=size(A,1);
nu = size(B,2);
Ad = (eye(nx) - A*Ts/2)^-1 * (eye(nx) + A*Ts/2);
Bd = (eye(nx) - A*Ts/2)^-1 * B*Ts;
Q = diag([10 1 1]);
R = 0.0175;
[~,P,~] = lqr(A,B,Q,R);
beta = 0.01;
[Kopt,~,~] = lqr(A,B,Q,R);

%% Build the optimizer
% Load the network and the bounds
load('data_Mdl_small_v2.mat')
K=QuadRNN.K;
Npred = 10; %prediction horizon
% input constraints in the new coordinates
vIn=sdpvar(repmat(nu,1,Npred),ones(1,Npred));
% input constraints in the original coordinates
uOut=sdpvar(repmat(nu,1,Npred),ones(1,Npred));
Xmpc=sdpvar(repmat(nx,1,Npred+1),ones(1,Npred+1));
Xr=sdpvar(repmat(nx,1,Npred+1),ones(1,Npred+1));

constraints = [];
objective = 0;
%Create cells to store variables
xHidden = cell(Npred,numel(QuadRNN.LayerSizes));
sHidden = cell(Npred,numel(QuadRNN.LayerSizes));
zHidden = cell(Npred,numel(QuadRNN.LayerSizes)); %binaire
%Assign variables to the cells
for i_mpc =1:Npred
    for k =1:K-1
        xHidden{i_mpc,k} = sdpvar(QuadRNN.LayerSizes(k),1);
        sHidden{i_mpc,k} = sdpvar(QuadRNN.LayerSizes(k),1);
        zHidden{i_mpc,k} = binvar(QuadRNN.LayerSizes(k),1);
    end
end
% Impose constraint over the horizon
for i_mpc =1:Npred
    %Model Prediction constraints
    constraints = [constraints,...
        Xmpc{i_mpc+1} == Ad*Xmpc{i_mpc} + Bd*vIn{i_mpc}];
    %Input constraint over the ReLU-ANN
    for k=1:K-1
        if k==1
            constraints = [constraints, ...
                QuadRNN.LayerWeights{k}*[Xmpc{i_mpc}(2,1);Xmpc{i_mpc}(3,1);vIn{i_mpc}] ...
                + QuadRNN.LayerBiases{k} ...
                == xHidden{i_mpc,k}-sHidden{i_mpc,k}];
        else
            constraints = [constraints, ...
                QuadRNN.LayerWeights{k}*xHidden{i_mpc,k-1} + QuadRNN.LayerBiases{k}...
                == xHidden{i_mpc,k}-sHidden{i_mpc,k}];
        end
        constraints = [constraints, xHidden{i_mpc,k}>=0, sHidden{i_mpc,k}>=0];
        for j = 1:numel(xHidden{i_mpc,k}) %n_k
            constraints = [constraints,...
                xHidden{i_mpc,k}(j)<= QuadRNN.UpBound{k}(j)*zHidden{i_mpc,k}(j),...
                sHidden{i_mpc,k}(j)<= -QuadRNN.LowBound{k}(j)*(1-zHidden{i_mpc,k}(j)),...
                ];
        end
    end
    constraints = [constraints,...
        QuadRNN.LayerWeights{K}*xHidden{i_mpc,K-1} + QuadRNN.LayerBiases{K} ==  uOut{i_mpc},...
        uOut{i_mpc}<= umax-ep, -(umax-ep)<=uOut{i_mpc}]; %real input constraint
    %Calculate the objective
    objective = objective + (Xmpc{i_mpc}-Xr{i_mpc})'*Q*(Xmpc{i_mpc}-Xr{i_mpc})+...
        vIn{i_mpc}'*R*vIn{i_mpc};
end
% buffer variables
res_vin=sdpvar(nu,Npred);
res_uOut=sdpvar(nu,Npred);
res_X = sdpvar(nx*Npred,1);
for i=1:Npred
    res_vin(:,i)= vIn{i} ;
    res_uOut(:,i)= uOut{i} ;
    res_X(nx*(i-1)+1:nx*i,1)=Xmpc{i};
end
options = sdpsettings('solver','cplex','verbose',0,'usex0',1); %use initial guess
parameters_opt={Xmpc{1},Xr{1:Npred}};
output={vIn{1},objective,res_vin,res_uOut,res_X};
controller=optimizer(constraints,objective,options,parameters_opt,output);

%% simulation
X0 = [-0.5;-0.15;-0.2];
tsim = 30;
Nsim = round(tsim/Ts);
tt=linspace(0,tsim,Nsim);
X_ref= zeros(nx,Nsim+1);
%Time varying reference
for t=1:size(X_ref,2)
    if t<=10/Ts
        X_ref(1,t)=0.5;
    else
        if t<=20/Ts
            X_ref(1,t)=1.5;
        else
            X_ref(1,t)=0.5;
        end
    end
end

%% simulation variables
Xsim = zeros(nx,Nsim+1);
Xsim(:,1) = X0;
vsim = zeros(nu, Nsim);
usim = zeros(nu, Nsim);
simRK4.vsim_pred = zeros(nu*Npred,Nsim);
simRK4.usim_pred = zeros(nu*Npred,Nsim);
simRK4.X_pred=zeros(Npred*nx,Nsim);
%% warm up
Ask2proceed
disp('warming up, intitializing the first solution')
i=1;
controller_input = {Xsim(:,i)};
for cnt=0:Npred-1
    if (i+cnt)>=length(tt)
        temp=length(tt);
    else
        temp=i+cnt;
    end
    controller_input{end+1}=X_ref(:,temp);
end
[res,~,~,~,controller] = controller(controller_input);

%% Simulation
disp('simulation started')
for i=1:Nsim
    i
    % collect the parameters for the optimizer
    controller_input = {Xsim(:,i)};
    for cnt=0:Npred-1
        if (i+cnt)>=length(tt)
            temp=length(tt);
        else
            temp=i+cnt;
        end
        controller_input{end+1}=X_ref(:,temp);
    end
    %Compute the control
    tic;
    [res,~,~,~,controller] = controller(controller_input);
    vsim(i)=res{1};
    comptime(i)=toc;
    obj_all(i) = res{2};
    if isnan(vsim(i))
        error('infeasible') %return error if control not found
    end
    %Save data
    simRK4.vsim_pred(:,i)=res{3}';
    simRK4.usim_pred(:,i)=res{4}';
    simRK4.X_pred(:,i)=res{5}';
    usim(i) = FL_quad1D([Xsim(2,i),Xsim(3,i),vsim(i)]);
    Xsim(:,i+1) = Ad*Xsim(:,i)+Bd*vsim(i);
end
beep; %wake me up when the simulation is over :D
%% real input, computed by the RNN approximation
for i=1:Nsim
    uNN(i) =  myRnn.eval([Xsim(2,i),Xsim(3,i),vsim(i)]');
end
%% Plot

close all
figure
set(gcf,'color','w');

subplot(3,1,1)
plot(tt, Xsim(1,1:end-1));
hold on
plot(tt, Xsim(2,1:end-1));
plot(tt, Xsim(3,1:end-1));
legend({'x_1','x_2','x_3'})
ylabel('states')
box on

subplot(3,1,2)
plot(tt,vsim(1,:));
ylabel('virtual input')
box on


subplot(3,1,3)
yline(umax,'color','b','linewidth',1)
hold on
yline(-umax,'color','b','linewidth',1)
yline(umax-ep,'color','r','linewidth',1)
yline(-umax+ep,'color','r','linewidth',1)

stairs(tt,usim(1,:),'k');
plot(tt,uNN(1,:),'k.-','markersize',10,'markerfacecolor','none');

ylabel('real input')
xlabel('time (s)')
box on


figure
plot(comptime)
ylabel('computation time (s)')

figure %reference tracking
plot(tt, Xsim(1,1:end-1));
hold on
plot(tt, X_ref(1,1:end-1));

