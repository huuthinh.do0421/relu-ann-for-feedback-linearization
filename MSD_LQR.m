% Stabilization with guaranteed safety using Control Lyapunov–Barrier Function
% Romdlony et al (2016)
% Example 1
% LQR k(z)
clc
clear
close all
addpath(genpath('tools'))

%% systems
A = [0 1;
    0 0];
B = [0;1];
% discretization
Ts=0.02;
nx=size(A,1);
nu = size(B,2);
Ad = (eye(nx) - A*Ts/2)^-1 * (eye(nx) + A*Ts/2);
Bd = (eye(nx) - A*Ts/2)^-1 * B*Ts;
umax=5;
%% Control design
% Some CLF
Q = diag([1 1]);
R = 100;
[~,P,~] = lqr(A,B,Q,R);
beta = 0.001;
gamma = 4;
[Kopt,~,~] = lqr(A,B,diag([10 1]),0.5);
%% simulation
X0 = [0 ;4.];
tsim = 10;
Nsim = round(tsim/Ts);
tt=linspace(0,tsim,Nsim);
Xsim = zeros(nx,Nsim+1);
Xsim(:,1) = X0;
vsim = zeros(nu, Nsim);
usim = zeros(nu, Nsim);
Xo = [1.5; 1];
Ro= 0.8;

for i=1:Nsim
    tic;
    vsim(i) =-Kopt* Xsim(:,i);
    comptime(i)=toc;
    
    i
    usim(i) = FL_Nonlinear_MSD([Xsim(1,i),Xsim(2,i),vsim(i)]');
    Xsim(:,i+1) = Ad*Xsim(:,i)+Bd*vsim(i);
end
for i=1:size(Xsim,2)-1
    Vclf(i)=Xsim(:,i)'*P*Xsim(:,i);
end

%%
close all
figure
subplot(1,2,1)
scatter(Xsim(1,1:end-1),Xsim(2,1:end-1))
hold on
Ellipsoid_patch2D(eye(2),Ro^2,Xo,'facealpha',0.1);
plot(Xsim(1,1:end-1),Xsim(2,1:end-1))
ylabel('x_2')
xlabel('x_1')


subplot(1,2,2)
plot(tt,Vclf)
ylabel('CLF V(z)')
figure
set(gcf,'color','w');
subplot(3,1,1)
plot(tt, Xsim(1,1:end-1));
hold on
plot(tt, Xsim(2,1:end-1));
legend({'x_1','x_2'})
ylabel('states')
box on

subplot(3,1,2)
stairs(tt,vsim(1,:));
ylabel('virtual input')
box on


subplot(3,1,3)
yline(umax,'color','b','linewidth',1)
hold on
yline(-umax,'color','b','linewidth',1)
pu=plot(tt,usim(1,:),'r','linewidth',2);
ylabel('real input')
xlabel('time (s)')
box on

figure
plot(comptime)
ylabel('computation time (s)')


