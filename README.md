# ReLU ANN for feedback linearization
 This repository includes the simulation MATLAB codes for the work with ReLU-ANN application to constraint characterization in feedback linearization control.

 The files are devided into two groups:

   * Mass-spring-damper training and simulations (noted with the prefix "MSD_" followed by the controller described in the manuscript);
   * Horizontal 1D quadrotor training and simulations (noted with "Quad1D_" prefix).

  Detailed explanation of the code can be found in [the document](./Extended_Version_LCSS_CDC2024.pdf).

## Required Matlab toolbox
*   [ ] [MPT3](https://www.mpt3.org/)
*   [ ] Yalmip (Usually attached with MPT3)
*   [ ] CPLEX solver 


## License
[![License: CC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/)



