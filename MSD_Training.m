% Stabilization with guaranteed safety using Control Lyapunov–Barrier Function
% Romdlony et al (2016)
% Example 1
clc
clear
close all
addpath(genpath('tools'))
%% Create NN fitting for the linearization law
% create data
max_box =[5;5;6];
Nsamp = 25;
v_train = linspace(-max_box(3),max_box(3),Nsamp);
x2_train = linspace(-max_box(2),max_box(2),Nsamp);
x1_train = linspace(-max_box(1),max_box(1),Nsamp);
Xtrain = [];
Ytrain = [];
for x1 = x1_train
    for x2 = x2_train
        for x3 = v_train
            ytmp = FL_Nonlinear_MSD([x1 x2 x3]');
            if isreal(ytmp)
                Xtrain = [Xtrain; [x1 x2 x3]];
                Ytrain = [Ytrain;ytmp];
            end
        end
    end
end

%% training
hiddenLayers=[20 20 20];
Mdl = fitrnet(Xtrain,Ytrain,"Standardize",false, ...
    "LayerSizes",hiddenLayers,'verbose',1);
fprintf('training time %.5f (seconds)\n',sum(Mdl.TrainingHistory.Time))
%% check the model
close all
Box_ws = Polyhedron('ub', max_box,'lb', -max_box);
ThinhRNN = MyRNN(Mdl);
XTest =  cprnd(1000,Box_ws.A,Box_ws.b);
YTest_thinh = zeros(size(XTest,1),1);
for i=1:size(XTest,1)
    YTest_thinh(i) = ThinhRNN.eval(XTest(i,:)');
    Ytrue(i) = FL_Nonlinear_MSD(XTest(i,:)');
end
YTest_matlab = Mdl.predict(XTest);
figure
plot(Ytrue,Ytrue,'b','linewidth',2);
hold on
scatter(Ytrue,YTest_thinh,25,'filled','markerfacecolor','g');
scatter(Ytrue,YTest_matlab,10,'filled','markerfacecolor','r');

%% estimate the error bound with pso
close all
% PSO initialization
init.K_max = 800; % total search steps
init.N = 500; %number of particles
init.bounds = Box_ws;
init.acc_constant = [1 50]*0.001; 
init.RNN  = MyRNN(Mdl);
init.TrueFunc = @FL_Nonlinear_MSD;
init.epsilon = 1e-8;
% reintialization flag
init.reInitFlag=true;
init.reInitRadius=1e-6;

pso_1=PSO(init);
obj_test=[];
figure
hold on
xlim([0 init.K_max])
for i=1:init.K_max
    disp(['loop ' num2str(i)]);
    pso_1.runPSO();
    obj_test = [obj_test, pso_1.g_best_objective];
    scatter(i,obj_test(end),10,'filled','markerfacecolor','b')
    drawnow
    ylabel('Largest error found')
    xlabel('Search step')
end
disp('_________________________________________________')
fprintf('The largest error found %.5f\n',obj_test(end))



